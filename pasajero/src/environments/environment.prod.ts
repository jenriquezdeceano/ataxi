export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyAWe4190ABrKo1G4P26rkHGDjV8Lh9uN_w",
    authDomain: "pidetaxi-8554a.firebaseapp.com",
    databaseURL: "https://pidetaxi-8554a.firebaseio.com",
    projectId: "pidetaxi-8554a",
    storageBucket: "pidetaxi-8554a.appspot.com",
    messagingSenderId: "712388158437",
    appId: "1:712388158437:web:4db132691a86d4dab7050c",
    measurementId: "G-LCMTCJ1W2H"
  }
};

export let SHOW_VEHICLES_WITHIN = 5; // within 5km
export let POSITION_INTERVAL = 10000; // 2000ms
export let VEHICLE_LAST_ACTIVE_LIMIT = 60000; // 60s

export let DEAL_STATUS_PENDING = 'pending';
export let DEAL_STATUS_ACCEPTED = 'accepted';
export let TRIP_STATUS_GOING = 'going';
export let TRIP_STATUS_FINISHED = 'finished';
export let DEAL_TIMEOUT = 20000; // 20s

export let EMAIL_VERIFICATION_ENABLED = true; // send verification email after user register
export let ENABLE_SIGNUP = true;
export let SOS = "+919500707757";
export let DEFAULT_AVATAR = "http://placehold.it/150x150";